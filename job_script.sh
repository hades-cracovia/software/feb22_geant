#!/bin/bash

echo "Running job_script_p4500p.sh"
date

echo "inputs:"
echo " pattern=$pattern"
echo " odir=$odir"

if [[ -n ${SLURM_ARRAY_TASK_ID} ]]; then
    echo "ARRAY: ${SLURM_ARRAY_TASK_ID} in ${pattern}"
    line=$(sed -n $((${SLURM_ARRAY_TASK_ID}+1))p ${pattern})
    IFS=' ' read -ra linearr <<< "$line"
    file=${linearr[0]}
    sodir=${linearr[1]}
else
    echo "NO ARRAY"
    file=$pattern
fi

[ -e job_profile.sh ] && . job_profile.sh

mkdir -p $odir/$sodir
odir=$(readlink -e $odir/$sodir)

#file=$(readlink -e $file)

echo file=$file
echo events=$events
echo odir=$odir

crashfile=$odir/crash_await_${SLURM_ARRAY_JOB_ID}_${SLURM_ARRAY_TASK_ID}.txt
echo $file > $crashfile

root -b -q

card_file_base=simul_p4500p_fwdet_feb22
card_file=/tmp/geaini_${card_file_base}__$(basename ${file} .evt).ini

date

truncate -s0 ${odir}/$(basename ${file} .evt)_1.root

# update placeholders and generate temporary file
echo "*********************************************"
echo "*********************************************"

sed \
    -e "s|@wd@|$(pwd)|g" \
    -e "s|@input@|${file}|" \
    -e "s|@output@|$(basename ${file} .evt)_.root|" \
    ${card_file_base}.dat | tee ${card_file}

echo "*********************************************"
echo "*********************************************"


if [ -z ${SLURM_TMPDIR+x} ]; then
    echo "SLURM_TMPDIR is unset"
    tmpdirname=$(mktemp -d)
else
    echo "SLURM_TMPDIR is set to '$SLURM_TMPDIR'"
    tmpdirname=${SLURM_TMPDIR}
fi

cd ${tmpdirname}
pwd

if [ -z ${HGEANT_DIR} ]; then
    echo hgeant -b -c -f ${card_file}
    time hgeant -b -c -f ${card_file}
else
    echo ${HGEANT_DIR}/hgeant -b -c -f ${card_file}
    time ${HGEANT_DIR}/hgeant -b -c -f ${card_file}
fi

mv $(basename ${file} .evt)_*.root ${odir}/ -v

cd -
rm -rfv ${tmpdirname}
rm -fv ${card_file}
rm -v $crashfile
