# Clone repository

```bash
git clone git@git.gsi.de:hades-cracovia/software/feb22_geant.git
```
or
```bash
git clone https://git.gsi.de/hades-cracovia/software/feb22_geant.git
```

# Prepare inputs

This is Rafal's way of doing this and differs from what Jochan uses.

1. Make directory for file lists with evt files
```bash
mkdir lists
```
2. Create list of inputs in a single file list
```bash
readlinke -e ../path/to/*.evt > lists/list1
```

3. Send jobs to farm
```bash
./run_jobs.py -d out lists/list1 -a
```
where:
* `-d out` - is an output directory
* `-a` - send as array

